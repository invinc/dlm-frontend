module.exports = function(grunt) {
  require("load-grunt-tasks")(grunt);

  grunt.initConfig({
    "nodewebkit": {
      options: {
        version: '0.12.2',
        platforms: ['linux64'], // Платформы, под которые будет строиться наше приложение
        buildDir: './build', // Путь, по которому будет располагаться построенное приложение
      },
      src: './app/**/*' // Путь, по которому располагаются исходные коды приложения
    },
    "less": {
       development: {
           options: {
               paths: ["app/styles/css"]
           },
           files: {"app/styles/css/style.css": "app/styles/less/style.less"}
       }
   },
    "babel": {
        dist: {
          files: [{
              expand: true,
              cwd: "app/scripts/src",
              src: "*.js",
              dest: 'app/scripts/',
              ext: ".js"
          }]
        }
    },
    "watch": {
      node: {
        files: ['app/*/**'],
        tasks: ['less'],
        options: {
          spawn: false,
        }
      }
    }
  });

  grunt.registerTask('default', ['less', 'watch']);
};
