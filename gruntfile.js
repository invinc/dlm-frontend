module.exports = function(grunt) {
  require("load-grunt-tasks")(grunt);
  grunt.loadNpmTasks("grunt-browserify");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.initConfig({
    "nodewebkit": {
      options: {
        version: '0.12.2',
        platforms: ['linux64'], // Платформы, под которые будет строиться наше приложение
        buildDir: './build', // Путь, по которому будет располагаться построенное приложение
      },
      src: './app/**/*' // Путь, по которому располагаются исходные коды приложения
    },
    "less": {
       development: {
           options: {
               paths: ["app/styles/css"]
           },
           files: {"app/styles/css/style.css": "app/styles/less/style.less"}
       }
   },
   browserify: {
         dist: {
            options: {
               browserifyOptions: {
                    debug: true
               },
               transform: [
                  ["babelify", {
                     loose: "all",
                     stage: 1
                  }]
               ]
            },
            files: {
               // if the source file has an extension of es6 then
               // we change the name of the source file accordingly.
               // The result file's extension is always .js
               "app/dist/scripts.js": ["app/scripts/*"]
            }
         }
      },
/*
    "babel": {
        dist: {
          files: [{
              expand: true,
              cwd: "app/scripts/src",
              src: "*.js",
              dest: 'app/scripts/',
              ext: ".js"
          }]
        }
    },
*/
    watch: {
      node: {
        files: ['app/*/**'],
        tasks: ['browserify:dist'], //, 'less'
        options: {
          spawn: false,
        }
      }
    }
  });

  grunt.registerTask('default', ['browserify', 'less', 'watch']);
};
