
var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: {
   app: "./app/scripts/app.js",
   vendor: ["react", "underscore", "multimethod", "datascript", "jquery", "dagre", "react-dnd", "react-router","formsy-react"],
  },
  output: {
      path: path.join(__dirname, 'app', 'dist'),
      filename: 'bundle.js',
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(true),
    new webpack.optimize.CommonsChunkPlugin("vendor","vendor.bundle.js"),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ],
  module: {
    loaders: [{
      test: /\.jsx?$/,
      loaders: ['babel'],
      include: path.join(__dirname, 'app','scripts')
    }]
  }
};
