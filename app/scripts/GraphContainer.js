import React, { Component, PropTypes } from 'react';
import { DropTarget } from 'react-dnd';
import Graph from './workspace';
import {select} from './db_funcs';

const graphTarget = {
  drop(props, monitor) {
      console.log(monitor.isOver({shallow: true}), monitor.canDrop(), monitor.didDrop(),"drop graph target");
    if (monitor.didDrop()) {
        return;
    }
    let offset = monitor.getClientOffset();
    let [x,y] = (offset ? [offset.x, offset.y] : [-1,-1]);
    return {
        type: "canvas",
        x,y
    };
  }
};

class GraphContainer extends Component {
    render () {
        let { canDrop, isOver, connectDropTarget } = this.props;
        console.log(canDrop, isOver, "container render");
        return connectDropTarget(
            <div className={this.props.className} id={this.props.id}>
                <svg className="canvas">
                    <Graph/>
                </svg>
            </div>
        );
    };
};
export default DropTarget("state", graphTarget, (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}, {arePropsEqual: () => false})(GraphContainer);
