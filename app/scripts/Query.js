import React, { Component } from 'react';
import Formsy  from 'formsy-react';
import {add, select, model, edit} from './db_funcs';
import FormPredict from './FormPredict';
import Router, {Link, RouteHandler} from 'react-router';



export default class Query extends Component {
     select(id) {
        edit({type:"predict", id: id, selected: true});
    }
    render () {
        let query = select({type: "predict"});
        return (
        <div className="column">
            <div className="ui borderless menu top attached segment tertiary teal">
                <span className="header item">Запрос к модели</span>
                <Link to="workspace_compact" params={{id: this.props.params.id}} className="item right" id="show_query" activeClassName="">
                    <i className="expand icon"></i>
                    К элементам модели
                </Link>
            </div>
            <FormPredict/>
            <div className="ui bottom attached two column grid segment" id="queries_model">
                <div className="column">
                    <div className="ui link list">
                        {query.map(({id, selected, time}) => (<Link  onClick={this.select.bind(this,id)} key={id} to="query" params={{queryId:id, id: model.openModel}} className={selected ? "active item" : "item"}>{time.toDateString()}</Link>))}
                    </div>
                </div>
            </div>
        </div>);
    }
}
