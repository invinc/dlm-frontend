import React, { Component } from 'react';
import Router, {Link, RouteHandler} from 'react-router';
import AddEntity from './AddEntity';
import EntityColumn from './EntityColumn';
import GraphContainer from './GraphContainer';
import {model, selectModel} from './db_funcs';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd/modules/backends/HTML5';



class WorkspaceCompact extends Component {
    render () {
        //console.log("workspace_compact render");
        selectModel(model, this.props.params.id);
        return (
            <div className="ui two column page grid c_content">
                <div className="column">
                    <div className="ui borderless menu top attached segment tertiary orange">
                        <span className="header item">Граф модели</span>
                        <Link to="workspace_full" params={{id: this.props.params.id}} className="item right" id="expand_workspace">
                            <i className="expand icon"></i>
                            На весь экран
                        </Link>
                    </div>
                    <GraphContainer className="ui bottom attached segment graph" id="graph_compact"></GraphContainer>
                </div>
                <RouteHandler/>
            </div>
        )
    }
}
export default DragDropContext(HTML5Backend)(WorkspaceCompact);
