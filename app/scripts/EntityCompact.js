import React, { Component } from 'react';
import Router, {Link, RouteHandler} from 'react-router';
import EntityColumn from './EntityColumn';

var types = ["state", "input", "output"];
export default class WorkspaceCompact extends Component {
    render () {
        return (
                <div className="column">
                    <div className="ui borderless menu top attached segment tertiary teal">
                        <span className="header item">Элементы модели</span>
                        <Link to="workspace_compact_with_queries" params={{id: this.props.params.id}} className="item right" id="show_query">
                            <i className="expand icon"></i>
                            Показать запрос
                        </Link>
                    </div>
                    <div className="ui bottom attached three column grid segment" id="elements_model">
                        {types.map(t => (<div key={t} className="column"><EntityColumn type={t} allActiveTab={true}/></div>))}
                    </div>
                </div>
        )
    }
}
