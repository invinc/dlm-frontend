import React, { Component } from 'react';
import {test, getNearestPoints, getMidOfLine, getSlopeCoeff} from './positions';
import Output from './Output';
import DeleteCircle from './DeleteComponentGraph';
import cx from 'classnames';

class EdgesList extends Component {
    render() {
        return <g>{this.props.data.map(linked => <Edge key={linked.id} data={linked} />)}</g>
    }
};

class Edge extends Component {
    state = {clicked: false}
    getLinePositions (edgeData) {
        let [{x: startX, y: startY}, {x: endX, y: endY}] = getNearestPoints(edgeData.state, edgeData.next_state, 50);
        return {startX, startY, endX, endY};
    }
    getArrPositions (linePositions) {
        let [{x: nextX, y: nextY}, {x: endX, y: endY}] = test(linePositions, 10);
        let {endX:startX, endY: startY} = linePositions;
        return {startX, startY, nextX, nextY, endX, endY};
    }
    render () {
        let { data } = this.props;

        let {startX, startY, endX, endY} = this.getLinePositions(data),
            arrow = this.getArrPositions({startX, startY, endX, endY}),
            lineData = `M ${startX} ${startY} L ${endX}  ${endY}`,
            arrData = `M ${arrow.startX} ${arrow.startY} L ${arrow.nextX} ${arrow.nextY} L ${arrow.endX} ${arrow.endY} Z`;
        let [beginInput, endInput] = [{x:startX, y: startY}, {x: endX, y: endY}];
        let middle = getMidOfLine(beginInput, endInput);
        let coeff = getSlopeCoeff(beginInput, endInput);
        let className = cx({
            selected_edge: data.hasOwnProperty("step"),
            normal_edge: (!data.hasOwnProperty("step") && !this.state.clicked),
            clicked_edge: this.state.clicked
        });
        return (
            <g id="" className="edge">
                <g onClick={() => this.setState({clicked: !this.state.clicked})}>
                    {
                        this.state.clicked ?
                        <DeleteCircle key="1" disconnectFrom={{type: "state", ...data.state}} disconnectTo={{type: "input", ...data.input}} parentR={10} parentX={middle.x} parentY={middle.y}/>
                        : undefined
                    }
                    <path key="2" d={lineData} className={className}></path>
                    <path key="3" d={arrData} className={className}></path>
                    <text key="4" x={coeff < 0 ? middle.x : middle.x - 30} y={middle.y} fontFamily="Verdana" fontSize="32">{data.input.name}</text>
                </g>
                <Output beginInput={beginInput} endInput={endInput} data={data.output} nodeId={data.id}/>
            </g>
        );
    }
};


export default EdgesList;
