import _, {groupBy,first} from 'underscore';
import React, {Component} from 'react';
import $ from 'jquery';
import {topOfCircle, botOfCircle, lenOfLine, arrBegin, test, getMidOfLine} from './positions';
import dagre from 'dagre';
import {select, runQuery} from './db_funcs';
import StatesList from './StatesList';
import EdgesList from './EdgesList';




function getAllUniqState(model) {
    return _.uniq(_.flatten(model.map(({state, next_state}) => [state, next_state])).filter(s => s !== undefined), false, s => s.id);
}


function makeLayout(state, edge) {
    var g = new dagre.graphlib.Graph({ directed: true, multigraph: true });
    g.setGraph({ label: "Model" });
    g.setDefaultEdgeLabel(function() { return {}; });
    state.forEach(s => g.setNode(s.id, { label: s.id, width: 100, height: 100 }));
    edge.forEach(e => g.setEdge(e.state.id, e.next_state.id));
    dagre.layout(g);
    let stateWithCoord = {};
    state.forEach(s => {
        let {x,y} = g.node(s.id);
        stateWithCoord[s.id] = _.extend(_.clone(s), {x,y});
    });
    return stateWithCoord;
}

function replacePlaceholderStates (edges) {
    let count = -1;
    return edges.map(e => {
        if (e.next_state.id < 0) {
            e.next_state = {id: count--}
        }
        return e;
    });
}


export default class Graph extends Component {
    render () {
        let states = select({type: "state", onCanvas: true});
        let edges  = select({type: "node", withPlaceholder: true});
        console.log(JSON.stringify(edges), JSON.stringify(states), "render graph");
        edges = replacePlaceholderStates(edges);
        states = states.concat(edges.filter(e => e.next_state.id < 0).map(e => e.next_state));
        let layout = makeLayout(states, edges);
        console.log(JSON.stringify(layout), "render graph");
        let layoutMap = s => ({...s,x: layout[s.id].x, y: layout[s.id].y});
        edges = edges.map(e => {
            [e.state, e.next_state] = [e.state, e.next_state].map(layoutMap);
            return e;
        });
        var nextStates = groupBy(edges.map(e => ({...e.next_state, nodeId: e.id})), s => s.id);
        //console.log(JSON.stringify(nextStates));
        states = states.map(s => {
            let newState = layoutMap(s);
            if(nextStates[s.id]){
                return {...newState, nodeId: nextStates[s.id][0].nodeId};
            } else {
                return newState
            }
        });
        let selectedQuery = first(select({type: "predict",selected: true}));
        //console.log(JSON.stringify(selectedQuery));
        if (selectedQuery) {
            let res = runQuery(selectedQuery);
            res.forEach(step => {
                edges = edges.map(e => {
                    if (step.state == e.state.id && step.nextState == e.next_state.id && step.input == e.input.id) {
                        [e.isFinal, e.step] = [step.isFinal, step.step]
                    }
                    return e;
                });
                states = states.map(s => {
                    if (s.id == step.state) {
                        [s.initStep, s.isFinal] = [step.step, step.isFinal];
                    }
                    if (s.id == step.nextState) {
                        [s.endStep, s.isFinal] = [step.step, step.isFinal];
                    }
                    return s;
                });
            });
        }

        //console.log(JSON.stringify(edges), JSON.stringify(states), "render graph");
        return (
            <g id="min_wrapper" className="wrapper">
                <StatesList data={states} />
                <EdgesList data={edges} />
            </g>
        );
    }
};
