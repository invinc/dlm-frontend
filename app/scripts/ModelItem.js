import React, { Component } from 'react';
import Router from 'react-router';
import _, {flatten} from 'underscore';
var Link = Router.Link;
export default class ModelItem extends Component {
    render () {
        let name = this.props.name;
        if (this.props.selected) {
            let withoutNeedle = this.props.name
                .split(this.props.selected);
            console.log(withoutNeedle);
            name = flatten(withoutNeedle.map((s,i) => (i == withoutNeedle.length - 1 ? s : [s, <span key={i} className="selected_text">{this.props.selected}</span>])));
            console.log(name);

        }
        console.log(name);
        return (
            <tr>
                <td><Link to="workspace_compact" params={{id: this.props.id}}>{name}</Link></td>
                <td className="center aligned">{this.props.date}</td>
            </tr>
        );
    };
};
