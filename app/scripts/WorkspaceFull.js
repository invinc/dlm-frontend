import React, { Component } from 'react';
import AddEntity from './AddEntity';
import EntityColumn from './EntityColumn';
import {model, selectModel} from './db_funcs';
import GraphContainer from './GraphContainer';
import Router, {Link} from 'react-router';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd/modules/backends/HTML5';


var types = ["state", "input", "output"];

var header = {
    state: {
        text: "Состояния",
        icon: "cube icon"
    },
    input: {
        text: "Входы",
        icon: "sign in icon"
    },
    output:{
        text: "Выходы",
        icon: "sign out icon"
    }
};

class Header extends Component {
    render () {
        return (
            <a onClick={this.props.onClickFunc} className={this.props.activeTab == this.props.type ? "item active" : "item"} data-tab={this.props.type}>
                <i className={header[this.props.type].icon}></i>
                {header[this.props.type].text}
            </a>
        );
    }
}

@DragDropContext(HTML5Backend)
export default class WorkspaceFull extends Component {
    constructor(props) {
      super(props);
      this.state = {activeTab: "state"};
    }
    activateTab (t) {
        this.setState({activeTab: t});
    }
    render () {
        selectModel(model, this.props.params.id);
        return (
            <div className="ui dimmer modals page transition visible active">
                <div className="ui fullscreen modal transition scrolling visible active" id="workspace_modal">
                    <div className="ui borderless menu top attached segment tertiary orange">
                        <span className="header item">Граф модели</span>
                        <Link  to="workspace_compact" params={{id: this.props.params.id}} className="item right" id="compress_workspace">
                            <i className="compress icon"></i>
                            Обычный размер
                        </Link>
                    </div>
                    <GraphContainer className="content graph" id="graph_full"></GraphContainer>
                    <div className="ui right visible sidebar vertical c_model_items_sidebar">
                        <div className="ui top attached secondary pointing tabular three item teal icon menu">
                            {types.map(t => <Header onClickFunc={this.activateTab.bind(this, t)} key={t} type={t} activeTab={this.state.activeTab}/>)}
                        </div>
                        {types.map(t => <EntityColumn key={t} type={t} activeTab={this.state.activeTab}/>)}
                    </div>
                </div>
            </div>)
    }
}
