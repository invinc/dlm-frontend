import React, { Component } from 'react';
import ModelItem from './ModelItem';
import {model} from './db_funcs';
import {CanSubmitMixin, MyOwnInput} from './FormComponents';
import Formsy  from 'formsy-react';

let ModelsList = React.createClass({
    getInitialState() {
        return {selected: ""};
    },
    search (currentValues, isChanged) {
        console.log(currentValues, isChanged);
        this.setState({selected: currentValues.name});
    },
    render() {
          return (
            <div className="column">
                <div className="ui top attached segment tertiary blue">
                    <div className="ui header">Последние созданные модели</div>
                </div>
                <Formsy.Form ref="form" onChange={this.search}>
                    <div className="ui form attached segment">
                        <div className="ui transparent left icon input">
                            <MyOwnInput autoFocus name="name" type="text" placeholder="Начните вводить название созданной модели..."/>
                            <i className="search icon"></i>
                        </div>
                    </div>
                </Formsy.Form>
                <div className="ui attached segment" id="model_tbl">
                    <table className="ui very basic celled selected table">
                        <thead>
                            <tr>
                                <th className="thirteen wide">Модель</th>
                                <th className="three wide center aligned c_orderable c_title">
                                    Дата
                                    <i className="icon caret up cur"></i>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {model.models
                                .map((m, id) => ({model: m, id: id}))
                                .filter(m => {
                                    if (this.state.selected) {
                                        if (m.model.name.indexOf(this.state.selected) == -1) {
                                            return false;
                                        }
                                    }
                                    return true;
                                }).map(({model: m, id}) => <ModelItem key={id} id={id} name={m.name} selected={this.state.selected} date={m.time.toDateString()}/>)}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
});
export default ModelsList;
