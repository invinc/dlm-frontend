import Router, {Route, DefaultRoute, RouteHandler} from 'react-router';
import React from 'react';
import Home from './Home';
import WorkspaceFull from './WorkspaceFull';
import WorkspaceCompact from './WorkspaceCompact';
import EntityCompact from './EntityCompact';
import Query from './Query';

var routes = (
    <Route handler={RouteHandler}>
        <Route path="/" name="Home" handler={Home}></Route>
        <Route path="/workspace/:id/fullscreen" name="workspace_full" handler={WorkspaceFull}></Route>
        <Route path="/workspace/:id" name="workspace_compact" handler={WorkspaceCompact}>
            <DefaultRoute handler={EntityCompact}/>
            <Route path="queries" name="workspace_compact_with_queries" handler={Query}></Route>
            <Route path="query/:queryId" name="query" handler={Query}></Route>
        </Route>
    </Route>
);

var CurrentRoot =  Router.create({
  routes: routes,
  location: Router.HashLocation
});
export function renderUI() {
    React.render(<CurrentRoot/>, document.body);
}
export function runRoute () {
    CurrentRoot.run(Root => renderUI());
}
