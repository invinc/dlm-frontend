import React, { Component } from 'react';
import Formsy  from 'formsy-react';
import {MyOwnInput,CanSubmitMixin} from './FormComponents';
import {add} from './db_funcs';

var settings = {
    input: {
        placeholder: "Добавить вход",
        validationError: "Название входа должно быть непустой строкой"
    },
    output: {
        placeholder: "Добавить выход",
        validationError: "Название выхода должно быть непустой строкой"
    },
    state: {
        placeholder: "Добавить состояние",
        validationError: "Название состояние должно быть непустой строкой"
    },
};

var AddEntity = React.createClass({
    mixins: [CanSubmitMixin],
    save: function (data) {
        console.log(data);
        add({type: this.props.type, name: data.name});
        this.refs.form.reset();
    },
    render: function () {
        return (
            <Formsy.Form ref="form" onValidSubmit={this.save} onValid={this.enableForm} onInvalid={this.disableForm}>
                <div className="ui form">
                    <div className={this.addDisabledClass("ui action input")}>
                        <MyOwnInput name="name" type="text" validations="isExisty" validationError={settings[this.props.type].validationError} placeholder={settings[this.props.type].placeholder} required/>
                        <button className={this.addDisabledClass("ui teal icon button")}>
                            <i className="plus icon"></i>
                        </button>
                    </div>
                </div>
            </Formsy.Form>
        );
    }
});
export default AddEntity;
