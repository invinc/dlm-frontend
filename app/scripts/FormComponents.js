import React, { Component } from 'react';
import Formsy  from 'formsy-react';

var MyOwnInput = React.createClass({
  mixins: [Formsy.Mixin],
  changeValue: function (event) {
    this.setValue(event.currentTarget.value);
  },
  render: function () {
    return (<input {...this.props} onChange={this.changeValue} value={this.getValue() || this.props.initValue}/>);
  }
});

let MySelect = React.createClass({
    mixins: [Formsy.Mixin],
    changeValue: function (event) {
      this.setValue(event.currentTarget.value);
    },
    render: function () {
      return (
          <select {...this.props} onChange={this.changeValue} value={this.getValue() || this.props.initValue}>
            {this.props.options.map(({value, text}) => <option key={value} value={value}>{text}</option>)}
          </select>);
    }
})


var CanSubmitMixin = {
    getInitialState: function () {
        return {canSubmit: false};
    },
    enableForm: function () {
     this.setState({
       canSubmit: true
     });
   },
   disableForm: function () {
     this.setState({
       canSubmit: false
     });
   },
   addDisabledClass: function (className) {
       return (this.state.canSubmit ? className : className + " disabled");
   }
};
export {MyOwnInput,CanSubmitMixin, MySelect};
