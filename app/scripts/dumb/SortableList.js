import React, { Component, PropTypes } from 'react';
import { DragSource, DropTarget } from 'react-dnd';

const itemSource = {
  beginDrag(props) {
    return {
      type: props.type,
      id: props.id,
      index: props.index
    };
  },
  endDrag(props, monitor) {
    let item = monitor.getItem();
    let result = monitor.getDropResult();
    console.log(item, result);
  }
};

const itemTarget = {
  drop(props, monitor) {
    return  {
        id: props.id,
        type: props.type
    };
  },
  hover(props, monitor, component) {
    const ownId = props.id;
    const draggedId = monitor.getItem().id;
    const ownIndex = props.index;
    const draggedIndex = monitor.getItem().index;
    if (draggedId === ownId) {
      return;
    }
    // What is my rectangle on screen?
    const boundingRect = React.findDOMNode(component).getBoundingClientRect();
    // Where is the mouse right now?
    const clientOffset = monitor.getClientOffset();
    // Where is my vertical middle?
    const ownMiddleY = (boundingRect.bottom - boundingRect.top) / 2;
    // How many pixels to the top?
    const offsetY = clientOffset.y - boundingRect.top;

    // We only want to move when the mouse has crossed half of the item's height.
    // If we're dragging down, we want to move only if we're below 50%.
    // If we're dragging up, we want to move only if we're above 50%.

    // Moving down: exit if we're in upper half

    if (draggedIndex < ownIndex && offsetY < ownMiddleY) {
      return;
    }

        // Moving up: exit if we're in lower half
    if (draggedIndex > ownIndex && offsetY > ownMiddleY) {
         return;
    }

    // Time to actually perform the action!
    props.move(draggedIndex, ownIndex);
  }
};

function collect(connect, monitor) {
  return {
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging()
  };
}

function collectTarget (connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}
const arePropsEqual = {arePropsEqual: () => false};
class SortableListItem extends Component {
    render () {
        let {isDragging, connectDragSource, text, canDrop, isOver, index, connectDropTarget, remove} = this.props;
        //console.log(name, canDrop, isOver, isDragging);
        const opacity = isDragging ? 0 : 1;
        return connectDragSource(
            connectDropTarget(<div className="item" style={{opacity}}>
                <i className="circle thin teal icon"></i>
                <div className="content"><div className="header">{text}</div></div>
                <i className="ban icon" onClick={() => remove(index)}/>
            </div>)
        );
    }
}
export default DropTarget(props => props.type, itemTarget, collectTarget)(DragSource(props => props.type, itemSource, collect)(SortableListItem));
