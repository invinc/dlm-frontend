import React, { Component } from 'react';
import ModelsList from './ModelsList';
import AddModel from './AddModel';
export default class Home extends Component {
  render() {
    return (
        <div className="ui two column page grid c_content">
            <div className="column"></div>
            <div className="column">
                <div className="ui one column grid">
                    <AddModel/>
                    <ModelsList/>
                </div>
            </div>
        </div>
        )
    };
};
