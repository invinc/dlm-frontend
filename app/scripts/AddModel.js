import React, { Component } from 'react';
import Formsy  from 'formsy-react';
import {MyOwnInput} from './FormComponents';
import {model, addModel} from './db_funcs';


export default class AddModel extends Component {
  constructor(props) {
    super(props);
    this.state = {canSubmit: false};
  }
  save (data) {
      console.log(data);

      addModel(model, data.name);
      this.refs.form.reset();
  }
  render() {
    return (
        <div className="column">
            <div className="ui top attached segment tertiary green">
                <div className="ui header">Новая модель</div>
            </div>
            <Formsy.Form ref="form" onValidSubmit={this.save.bind(this)} onValid={() => this.setState({canSubmit: true})} onInvalid={() => this.setState({canSubmit: false})}>
                <div className="ui form bottom attached segment">
                    <div className={this.state.canSubmit ? "ui action input" : "ui action error input"}>
                        <MyOwnInput name="name" type="text" validations="isExisty" validationError="Название модели должно быть непустой строкой" placeholder="Название модели" required/>
                        <button type="submit" className={this.state.canSubmit ? "ui green animated button" : "ui disabled green animated button"}>
                            <div className="visible content">Создать</div>
                            <div className="hidden content"><i className="right arrow icon"></i></div>
                        </button>
                    </div>
                </div>
            </Formsy.Form>
        </div>
    );
  }
}
