import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import {topOfCircle, botOfCircle, lenOfLine, arrBegin, test, getMidOfLine} from './positions';
import DeleteCircle from './DeleteComponentGraph';

const PLACEHOLDER = "?";
const R = 40;
class StatesList extends Component {
    render() {
        return <g>{this.props.data.map(stateObj => <State key={stateObj.id} data={stateObj}/>)}</g>
    };
};

class State extends Component {
    state = {clicked: false}
    clickHandle () {
        if (this.props.data.id < 0) {
            return;
        }
        this.setState({clicked: (this.state.clicked ? false : true)});
    }
    getCircleClasses (canDrop, isOver, data) {
        let className = "";

        if (canDrop && isOver) {className += "hovered_node ";}
        data.id < 0 ? className += "placeholder_node" : className += "normal_node";
        if (data.hasOwnProperty('initStep') || data.hasOwnProperty('endStep')) {
            className += " selected_node";
        }
        if (this.state.clicked) {
            className += " clicked_node";
        }

        return className;
    }

    render () {
        let { canDrop, isOver, connectDropTarget, data } = this.props;
        //console.log(canDrop, isOver, "state render", data);
        return connectDropTarget(
            <g id={data.id} className="state_wrapper">
                {this.state.clicked
                    ? <DeleteCircle key="1" disconnectFrom={{type: "canvas"}} disconnectTo={{type: "state", id: data.id}} parentR={R} parentX={data.x} parentY={data.y}/>
                    : undefined
                }
                <circle key="2" cx={data.x} cy={data.y} r={R} className={this.getCircleClasses(canDrop, isOver, data)} onClick={this.clickHandle.bind(this)}></circle>
                <text key="3" x={data.x} y={data.y} fontFamily="Verdana" fontSize="32">{data.name ? data.name : PLACEHOLDER}</text>
            </g>
        );
    }
};

const stateTarget = {
  drop({data:{nodeId, id, x, y}}, monitor) {
      console.log(monitor.isOver({shallow: true}), monitor.canDrop(),"drop state target");
    return  (id < 0
        ? {
            type: "node",
            id: nodeId,
            x, y
        }
        : {
            type: "state",
            id, nodeId
        });
  }
};

State = DropTarget(({data:{nodeId, id}}) => (nodeId ? (id < 0 ? 'state' : ['input', 'state']) : 'input'), stateTarget, (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}, {arePropsEqual: () => false})(State);

export default StatesList;
