import React, { Component } from 'react';
import EntityItem from './EntityItem';
import {select} from './db_funcs';

export default class EntityList extends Component {
    render () {
        return (
            <div className="ui celled large selection list">
                {select({type: this.props.type}).map(e => <EntityItem key={e.id} id={e.id} name={e.name} type={this.props.type}/>)}
            </div>
        );
    };
};
