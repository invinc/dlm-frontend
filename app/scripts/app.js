require("../styles/less/style.less");

import {getSelectModelDb,
    getSelectModelConn,
    addModel,
    selectModel,
    dumpdb,
    add,
    edit,
    retract,
    select,
    connect,
    disconnect,
    selectGraph,
    model,
    loadModel} from './db_funcs';
import {renderUI, runRoute} from './renderUI';
import $ from 'jquery';

$(document).ready(() => {
    loadModel();
    runRoute();
});
