import * as ds from "datascript";
import multimethod from "multimethod";
import {renderUI} from './renderUI';
import _, {isArray, first, isUndefined, indexOf, pairs, object, has, contains} from 'underscore';
import {typeQueries} from './common';


var model = {
  models: [],
  openModel: undefined
};
function dsToJSON (scheme) {
  return function () {
    var trs = ds.q("[:find ?e ?a ?v :in $ :where [?e ?a ?v]]", ds.db(this))
        .map(([e, a, v]) => [":db/add", e, a, v]);
    return {
      "datascript": {
        transacts: trs,
        scheme: scheme
      }
    };
  }
};

const transactHandler = (transactData) => {
    console.log("transactHandler");
    persistModel();
    renderUI();
};

function persistModel () {
    const jsonStr = JSON.stringify(model);
    localStorage.setItem("model", jsonStr);
}

function loadModel () {
    let savedData = localStorage.getItem("model");    
    if (savedData) {
        let loadedModel = JSON.parse(savedData,(k,v) => {
          if (k == 'datascript') {
            var db = ds.create_conn(v.scheme);
            db.toJSON = dsToJSON(v.scheme);
            ds.transact(db, v.transacts.map(([op, e, a, v]) => [op, e, a, (a == 'time' ? new Date(v) : v)]));
            ds.listen(db,transactHandler);
            return db;
          };
          if ((k == 'db' || k == 'query') && v.hasOwnProperty('datascript')) {
            return v.datascript;
          };
          if (k == 'time') {
            return new Date(v);
          };
          return v;
        });
        if (loadedModel) {
            model = loadedModel;
        }
    }
}

export {model, loadModel};
function getSelectModelDb() {
  return ds.db(model.models[model.openModel].db);
}

function getSelectModelConn() {
  return model.models[model.openModel].db;
}

function getSelectQueryDb() {
  return ds.db(model.models[model.openModel].query);
}

function getSelectQueryConn() {
  return model.models[model.openModel].query;
}



export function addModel(model, name) {;
  let scheme = {
    "state": {
      ":db/valueType": ":db.type/ref"
    },
    "input": {
      ":db/valueType": ":db.type/ref"
    },
    "next_state": {
      ":db/valueType": ":db.type/ref",
      ":db/cardinality": ":db.cardinality/many"
    },
    "output": {
      ":db/valueType": ":db.type/ref",
      ":db/cardinality": ":db.cardinality/many"
    }
  };
  let db = ds.create_conn(scheme);
  db.toJSON = dsToJSON(scheme);
  ds.listen(db, transactHandler);
  let schemeQuery = {
    "state": {
      ":db/valueType": ":db.type/ref"
    }
  };
  var dbQuery = ds.create_conn(scheme);
  dbQuery.toJSON = dsToJSON(schemeQuery);
  ds.listen(dbQuery, transactHandler);
  var newModel = {
    db: db,
    name: name,
    time: new Date(),
    query: dbQuery
  };
  model.models.push(newModel);
  transactHandler();
}

export function selectModel(model, number) {
  model.openModel = number;
}

export function dumpdb() {
  console.log(JSON.stringify(ds.q('[:find ?e ?a ?v :in $ :where [?e ?a ?v]]', getSelectModelDb())));
}

var mapAddObj = multimethod()
  .dispatch("type")
  .when('input', obj => ({"input/name": obj.name}))
  .when('state', obj => ({"state/name": obj.name}))
  .when('output', obj => ({"output/name": obj.name}))
  .when("predict", ({selected = false, state, inputs = [],type}) => ({selected: selected, state: state, inputs: inputs, time: new Date(),type}))
  .default(obj => {
  throw new Error("not implemented add for " + JSON.stringify(obj))
});

export function markAllQueryUnselected () {
    ds.q('[:find ?e :where [?e "selected" true]]', getSelectQueryDb())
        .map(([id]) => ds.transact(getSelectQueryConn(),[[":db/retract", id, "selected", true]]));
}

var mapEditObj = multimethod()
  .dispatch("type")
  .when('input', obj => ({"input/name": obj.name}))
  .when('state', obj => {
    let edit = {};
    ['name', 'x', 'y'].forEach(el => {
      if (obj.hasOwnProperty(el)) {
        edit["state/" + el] = obj[el];
      };
    });
    return edit;
  })
  .when('output', obj => ({"output/name": obj.name}))
  .when('predict', obj => {
      if (has(obj, "selected") && obj.selected) {
          markAllQueryUnselected();
      }
      return object(["state", "inputs", "selected"].filter(k => obj[k]).map(k => ([k, obj[k]])));
  })
  .default(obj => {
    throw new Error("not implemented edit for " + JSON.stringify(obj));
});

let getConn = type => (contains(typeQueries,type) ?  getSelectQueryConn() : getSelectModelConn());
export function add(obj) {
  var entity = mapAddObj(obj);
  entity[":db/id"] = -1;
  var {tempids:{"-1": id}} = ds.transact(getConn(obj.type), [entity]);
  return id;
}


export function edit(obj) {
  var id = obj.id;
  var entity = mapEditObj(obj);
  entity[":db/id"] = parseInt(id);
  ds.transact(getConn(obj.type), [entity]);
  return id;
}

var retract = multimethod()
  .dispatch("type")
  .when("input",  ({id}) => ds.transact(getSelectModelConn(), [[":db.fn/retractEntity", id]]))
  .when("output", ({id}) => ds.transact(getSelectModelConn(), [[":db.fn/retractEntity", id]]))
  .when("state",  ({id}) => ds.transact(getSelectModelConn(), [[":db.fn/retractEntity", id]]))
  .when("predict", ({id}) => ds.transact(getSelectQueryConn(), [[":db.fn/retractEntity", id]]))
  .default(() => {
  throw new Error("not implemented retract for " + JSON.stringify(arguments));
});

let selectById = id => (!isUndefined(id) ? (isArray(id) ? '[?e ...]' : '?e') : '');

var select = multimethod()
  .dispatch("type")
  .when("input", ({onCanvas, id:inputId}) => {
      let res = ds.q(`[:find ?e ?name
        :in $ ${selectById(inputId)}
        :where [?e "input/name" ?name]
            ${ !isUndefined(onCanvas) ? (onCanvas ? '[?node "input" ?e]' : '[(missing? $ ?e "_input")]') : '' }]`, getSelectModelDb(), inputId);
    res =  res.map(([id, name]) => ({id, name}));
    return res;
  })
  .when("state", ({onCanvas, id}) => {
    return ds.q(`[:find ?e ?name ?x ?y
        :in $ ${selectById(id)}
        :where
            [?e "state/name" ?name]
            [(get-else $ ?e "state/x" -1) ?x]
            [(get-else $ ?e "state/y" -1) ?y]
            ${!isUndefined(onCanvas) ? (onCanvas ? '[(> ?x 0)] [(> ?y 0)]' : '[(< ?x 0)] [(< ?y 0)]') : ""}]`, getSelectModelDb(), id)
        .map(([id, name, x, y]) => ({id, name, x, y}))
  })
  .when("output", ({id}) => ds.q(`[:find ?e ?name
        :in $ ${selectById(id)}
        :where [?e "output/name" ?name]]`, getSelectModelDb(), id)
        .map(([id, name]) => ({id, name}))
  )
  .default(() => {
    throw new Error("not implemented select for " + JSON.stringify(arguments));
});

select.when("node", ({withPlaceholder = false}) => {
     let res = ds.q(`[:find ?e ?state ?next-state ?input ?output
                        :in $ :where [?e "state" ?state]
                            [?e "input" ?input]
                            ${withPlaceholder
                                ? '[(get-else $ ?e "next_state" -1) ?next-state] [(get-else $ ?e "output" -1) ?output]'
                                : '[?e "next_state" ?next-state] [?e "output" ?output]'
                          }]`, getSelectModelDb());
  //console.log(JSON.stringify(res, null, "\t"));
    return res.map(([nodeId, stateId, nextStateId, inputId, outputId]) => {
           return {
               id: nodeId,
               state:      first(select({type: "state", id: stateId})),
               next_state: (nextStateId < 0 ? {id: nextStateId} : first(select({type: "state", id: nextStateId}))),
               input:      first(select({type: "input", id: inputId})),
               output:     (outputId < 0 ? {id: outputId} : first(select({type:"output", id: outputId})))
           };
      });
});

select.when("predict",({selected}) => {
    //console.log(selected);
    let res = ds.q(`[:find ?e ?inputs ?state ?selected ?time
         :in $ ?type ?only-selected
         :where [?e "type" ?type]
                [?e "state" ?state]
                [?e "inputs" ?inputs]
                [?e "time" ?time]
                [(get-else $ ?e "selected" false) ?selected]
                ${ !isUndefined(selected) ? '[(= ?selected ?only-selected)]' : ''}]`, getSelectQueryDb(), "predict", selected);
     return res.map(([id, inputs, stateId, selected, time]) => {
         let inputsData =  inputs.map(inputId => first(select({type: "input",id: inputId})));
        //console.log(stateId,"state in predict");
         return {
           id,selected, time,
           type: "predict",
           state: first(select({type: "state", id: stateId})),
           inputs: inputsData
       }
   });
});

var connect = multimethod()
  .dispatch((...anything) => anything.map(item => item.type))
  .when(["state", "input"], ({id: stateId}, {id: inputId}) => {
    var {tempids: {"-1": id}} = ds.transact(getSelectModelConn(), [{
      ":db/id": -1,
      "state": stateId,
      "input": inputId
    }]);
    return id;
  })
  .when(["node", "state"], ({id: nodeId}, {id: stateId}) => {
    ds.transact(getSelectModelConn(), [{
      ":db/id": nodeId,
      "next_state": stateId
    }]);
    return nodeId;
  })
  .when(["node", "output"], ({id: nodeId}, {id: outputId}) => {
    ds.transact(getSelectModelConn(), [{
      ":db/id": nodeId,
      "output": outputId
    }]);
    return nodeId;
  })
  .default(() => {
    throw new Error("not implemented connect for " + JSON.stringify(arguments));
  });

let dropCoordsState = (id) => {
    let state = first(select({id, type: "state"}));
    return [
        [":db/retract", state.id, "state/x", state.x],
        [":db/retract", state.id, "state/y", state.y]
    ];
};
let getNodesByState = (id) => {
    return {
        forState: ds.q(`[:find [?e ...] :in $ ?state :where [?e "state" ?state]]`,
      getSelectModelDb(), id),
        forNextState: ds.q(`[:find [?e ...] :in $ ?state :where [?e "next_state" ?state]]`,
        getSelectModelDb(), id)
    };
};

var disconnect = multimethod()
  .dispatch((...anything) => anything.map(item => item.type))
  .when(["canvas", "state"], (canvas,state) => {
      let ids = getNodesByState(state.id);
      console.log(ids);
      let trs = ids.forState.map(i => [":db.fn/retractEntity", i])
        .concat(ids.forNextState.map(i => [":db/retract", i, "next_state", state.id]));
      ds.transact(getSelectModelConn(), trs.concat(dropCoordsState(state.id)));
      dumpdb();
  })
  .when(["state", "input"], ({id: stateId}, {id: inputId}) => {
    const [id, nextState] = ds.q(`[:find [?e ?next-state] :in $ ?state ?input :where [?e "state" ?state] [?e "input" ?input] [(get-else $ ?e "next_state" -1) ?next-state]]`, getSelectModelDb(), stateId, inputId);

    let trs = [[":db.fn/retractEntity", id]];
    ds.transact(getSelectModelConn(), trs);
  })
  .when(["node", "state"], ({id: nodeId}, {id: stateId}) => {
    ds.transact(getSelectModelConn(), [[":db/retract", nodeId, "next_state", stateId]]);
  })
  .when(["node", "output"], ({id: nodeId}, {id: outputId}) => {
    ds.transact(getSelectModelConn(), [[":db/retract", nodeId, "output", outputId]]);
  })
  .default((a,b) => {
    throw new Error("not implemented disconnect for " + JSON.stringify([a,b]));
  });

let runQuery = multimethod()
    .dispatch(q => q.type)
    .when("predict", query => prediction(query.inputs.map(i => i.id), query.state.id));

let replace = multimethod()
     .dispatch((...anything) => anything.map(item => item.type))
     .when(["state", "state"], ({id: oldId}, {id: newId}) => {
         let ids = getNodesByState(oldId);
         console.log(ids);
         let trs = ids.forState.map(id => [":db/add", id, "state", newId]);
         let dropOldCoords = dropCoordsState(oldId);
         let addNewCorrds = dropOldCoords.map(([op,id,attr, value])=> [":db/add", newId, attr, value]);
         trs = trs
            .concat(dropOldCoords)
            .concat(addNewCorrds)
            .concat(ids.forNextState.map(id => [":db/retract", id, "next_state", oldId]))
            .concat(ids.forNextState.map(id => [":db/add", id, "next_state", newId]));
        console.log(trs);
         ds.transact(getSelectModelConn(), trs);
     })
     .default(() => {
         throw new Error("not implemented disconnect for " + JSON.stringify([a,b]));
    });

export {retract, select, connect, disconnect, runQuery, replace};

export function prediction(pathInput, stateInit) {
  var getListEl = function(a, index) {
    //console.log(a, index);
    if (a[index] != undefined) {
      return a[index];
    } else {
      return 0;
    }
  };
  var rulePrediction = `[
    [(step_to ?current-state ?inputs ?current-step ?index-func ?end-state ?end-input ?end-step)
    [(?index-func ?inputs ?current-step) ?end-input]
    [?e "state" ?current-state]
    [?e "input" ?end-input]
    [?e "next_state" ?end-state]]
  [(step_to ?current-state ?inputs ?current-step ?index-func ?end-state ?end-input ?next-step)
    [(?index-func ?inputs ?current-step) ?current-input]
    [?e "state" ?current-state]
    [?e "input" ?current-input]
    [?e "next_state" ?next-state]
    [(+ ?current-step 1) ?next-step]
    (step_to ?next-state ?inputs ?next-step ?index-func ?end-state ?end-input ?next-step)]]`;

  var predict = ds.q(`[:find ?end-state ?end-input ?end-step
                       :in $ % ?index ?inputs ?idx ?s0
                       :where (step_to ?s0 ?inputs ?idx ?index ?end-state ?end-input ?end-step) ]`,
    getSelectModelDb(), rulePrediction, getListEl, pathInput, 0, stateInit);
  let preState = stateInit;
  return predict.map(step => {
      let stepCount = (step[2] || 0);
      let stepObj = {
          state: preState,
          nextState: step[0],
          input: step[1],
          step: stepCount,
          isFinal: (stepCount == (pathInput.length - 1))
      };
      preState = stepObj.nextState;
      return stepObj;
  });
}

export function direction(stateInit, output) {
  /*
    var followsRule = '[ \
      [(followsBase ?e1 ?s1 ?s2 ?e2) \
        [?e1 "state" ?s1] \
        [?e1 "next_state" ?s2] \
        [?e2 "state" ?s2]] \
      [(follows ?e1 ?s1 ?s2 ?e2) \
        (followsBase ?e1 ?s1 ?s2 ?e2)] \
      [(follows ?e1 ?s1 ?s2 ?e2 ?e3) \
        (followsBase ?e1 ?s1 ?s ?e2) \
        (follows ?e2 ?s ?s2 ?e3)]]';
    var query = '[:find ?e1 ?e2 ?e3 :in $ % ?s1 ?s2 :where (follows ?e1 ?s1 ?s2 ?e2 ?e3)]';
  */
  var directionRule = '[ \
  [(directs ?state ?output) \
  ]]';
  return ds.q(query, getSelectModelDb(), followsRule, 1, 9);
}

export function diag(path) {
  var ruleDiag = `
  [(diag ?index-func ?outputs ?step ?node)
    [(?index-func ?outputs ?step) ?output]
    [?node "output" ?output]]
  [(diag ?index-func ?outputs ?step ?node)
    [(?index-func ?outputs ?step) ?output]
    [?node "output" ?output]
    [?node "state" ?state]
    [?pred-node "next_state" ?state]
    [(+ ?step 1) ?next-step]
    (diag ?index-func ?outputs ?next-step ?pred-node)]]`;
  var res = ds.q(`[:find ?node ?step0 :in $ ?outputs ?index ?step0 % :where (diag ?index ?outputs ?step0 ?node)]`,
    getSelectModelDb(), path, getListEl, 0, ruleDiag);
  return res;
}
