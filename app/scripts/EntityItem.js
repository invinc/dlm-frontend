import React, { Component, PropTypes } from 'react';
import { DragSource } from 'react-dnd';
import multimethod from 'multimethod';
import {edit, select, connect, disconnect, retract, replace} from './db_funcs';
import {CanSubmitMixin, MyOwnInput} from './FormComponents';
import Formsy  from 'formsy-react';

var settings = {
    input: {
        validationError: "Название входа должно быть непустой строкой"
    },
    output: {

        validationError: "Название выхода должно быть непустой строкой"
    },
    state: {
        validationError: "Название состояние должно быть непустой строкой"
    },
};

let endDragHandle = multimethod()
.dispatch((item,result) => [item.type, (result ? result.type : undefined)])
.when(["state", "canvas"], ({id, type}, {x,y}) => edit({id, type, x, y}))
.when(["input", "state"], (input, state) => {
    console.log(state, input);
    connect(state, input);
})
.when(["state", "node"], (state, node) => {
    console.log(node, state);
    let {x, y} = node;
    edit({...state, x, y});
    connect(node, state);
})
.when(["output", "node"], (output, node) => {
    connect(node, output);
})
.when(["output", "output"], (newOut, oldOut) => {
    let node = {type: "node",  id: oldOut.nodeId};
    disconnect(node, oldOut);
    connect(node, newOut);
})
.when(["state", "state"], (newState, oldState) => {
    replace(oldState, newState);
})
.default(() => {
    console.log(arguments,"Error");
});

const entitySource = {
  beginDrag(props) {
    return {
      type: props.type,
      id: props.id
    };
  },
  endDrag(props, monitor) {
    let item = monitor.getItem();
    let result = monitor.getDropResult();
    console.log(item, result);
    endDragHandle(item, result);
  }
};
function collect(connect, monitor) {
  return {
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging()
  };
}
let EntityItem = React.createClass({
    mixins: [CanSubmitMixin],
    save ({name}) {
        console.log(name);
        let {type, id} = this.props;
        edit({type, id, name});
    },
    toInput () {
        this.setState({isEdit: true});
    },
    toText () {
        this.setState({isEdit: false});
    },
    onValidHandle () {
        this.enableForm();
        this.refs.form.submit()
    },
    removeHandle () {
        let {type, id} = this.props;
        console.log(type, id);
        retract({type, id});
    },
    render () {
        const { isDragging, connectDragSource, name, type, id } = this.props;
        //console.log(this.props);
        return (
            connectDragSource(
                <div className="item">
                    <i className="circle thin teal icon"></i>
                    <div className="content">{
                            this.state.isEdit && !isDragging
                            ? (
                                <Formsy.Form ref="form" onValidSubmit={this.save} onValid={this.onValidHandle} onInvalid={this.disableForm}>
                                    <div className="ui small form">
                                        <div className="ui action input">
                                            <MyOwnInput autoFocus name="name" type="text" validations="isExisty" onBlur={this.toText} initValue={name} validationError={settings[this.props.type].validationError} required/>
                                        </div>
                                    </div>
                                </Formsy.Form>
                            )
                            : <div className="header">{name}</div>
                    }</div>
                    <i className="edit icon" onClick={this.toInput}/>
                    <i className="ban icon" onClick={this.removeHandle}/>
                </div>
            )
        );
    }
});
export default DragSource(props => props.type, entitySource, collect, {arePropsEqual: () => false})(EntityItem);
