import React, { Component } from 'react';
import EntityList from './EntityList';
import AddEntity from './AddEntity';
export default class EntityColumn extends Component {
    render () {
        //console.log("entity column render");
        var addClassName = (this.props.activeTab == this.props.type || this.props.allActiveTab ? " active" : "");
        return (
            <div className={"ui attached tab segment" + addClassName} data-tab={this.props.type}>
                <AddEntity type={this.props.type}/>
                <EntityList type={this.props.type}/>
            </div>
        );
    };
};
