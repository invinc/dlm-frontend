import React, { Component } from 'react';
import {topOfCircle, botOfCircle, lenOfLine, arrBegin, test, getMidOfLine} from './positions';
import {disconnect} from './db_funcs';
const R = 10;

export default class DeleteCircle extends Component {
    clickHandle () {
        console.log(this.props.disconnectFrom, this.props.disconnectTo);
        disconnect(this.props.disconnectFrom, this.props.disconnectTo);
    }
    render () {
        const RWithPadding = R * 1.5;
        const [x,y] = [this.props.parentX + this.props.parentR + RWithPadding, this.props.parentY - this.props.parentR - RWithPadding].map(p => (p < R ? R : p));
        const halfR = R / 2;
        const firstLine = {
            x1: x + halfR,
            x2: x - halfR,
            y1: y - halfR,
            y2: y + halfR,
        };
        const secondLine = {
            x1: x - halfR,
            y1: y - halfR,
            x2: x + halfR,
            y2: y + halfR,
        };
        return (
            <g className="delete-circle">
                <line {...firstLine}></line>
                <line {...secondLine}></line>
                <circle cx={x} cy={y} r={R} onClick={this.clickHandle.bind(this)}></circle>
            </g>
        );
    }
}
