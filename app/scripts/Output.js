import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import {getNearestPoints, getMidOfLine, getIntersect, minLenOfLine, getPerpendicularOffset, getSlopeCoeff} from './positions';
import DeleteCircle from './DeleteComponentGraph';
const PLACEHOLDER = "?";
const R = 15;
const offset = 25;


class Output extends Component {
    state = {clicked: false}
    render () {
        let {beginInput, endInput, data,  canDrop, isOver, connectDropTarget, nodeId} = this.props;
        let quarter = getMidOfLine(getMidOfLine(beginInput, endInput), endInput);
        let diff = getPerpendicularOffset(beginInput, endInput, (getSlopeCoeff(beginInput, endInput) < 0 ? - offset : offset));
        //console.log(canDrop, isOver);
        quarter.x = quarter.x + diff.x;
        quarter.y = quarter.y + diff.y;
        let intersects = getIntersect(quarter, endInput, R);
        let beginLine  = minLenOfLine([[intersects[0], endInput], [intersects[1], endInput]])[0];
        //console.log(beginLine);
        return connectDropTarget(<g className="output">
            {
                this.state.clicked
                    ? <DeleteCircle key="1" disconnectFrom={{type: "node", id: nodeId}} disconnectTo={{type: "output", ...data}} parentR={R} parentX={quarter.x} parentY={quarter.y}/>
                    : undefined
            }
            <g key="2" onClick={() => this.setState({clicked: (!this.state.clicked && data.id > 0)})}>
                <line x1={beginLine.x} y1={beginLine.y} x2={endInput.x} y2={endInput.y}></line>
                <circle cx={quarter.x} cy={quarter.y} r={R}></circle>
                <text x={quarter.x - R + 7} y={quarter.y + R - 5}>{data.name ? data.name : PLACEHOLDER}</text>
            </g>
        </g>);
    }
}


const outputTarget = {
  drop({nodeId, data:{id}}, monitor) {
    return  (id < 0
        ? {
            type: "node",
            id: nodeId
        }
        : {
            type: "output",
            id, nodeId
        });
  }
};

Output = DropTarget('output', outputTarget, (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    }
}, {arePropsEqual: () => false})(Output);
export default Output;
